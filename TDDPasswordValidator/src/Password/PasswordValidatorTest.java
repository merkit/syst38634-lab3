package Password;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PasswordValidatorTest {

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	public void setUp() throws Exception {
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckLength() {
		assertTrue("Wrong", PasswordValidator.checkLength("1234567891011"));
	}

	@Test
	public void testCheckLengthExceptional() {
		assertTrue("Wrong", !PasswordValidator.checkLength("123"));
	}
	@Test
	public void testCheckLengthBoundaryIn() {
		assertTrue("Wrong", PasswordValidator.checkLength("12345678"));
	}

	@Test
	public void testCheckLengthBoundaryOut() {
		assertTrue("Wrong", !PasswordValidator.checkLength("1234567"));
	}

	@Test
	public void testContainsDigits() {
		assertTrue("Wrong", PasswordValidator.containsDigits("12345678"));
	}
	@Test
	public void testContainsDigitsExceptional() {
		assertTrue("Wrong", !PasswordValidator.containsDigits("abcdefgh"));
	}

	@Test
	public void testContainsDigitsBoundaryIn() {
		assertTrue("Wrong", PasswordValidator.containsDigits("abcdef78"));
	}
	@Test
	public void testContainsDigitsBoundaryOut() {
		assertTrue("Wrong", !PasswordValidator.containsDigits("abcdefg8"));
	}

}
